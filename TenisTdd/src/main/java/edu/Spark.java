package edu;

import spark.ModelAndView;
import spark.template.velocity.VelocityTemplateEngine;

import java.util.HashMap;
import java.util.Map;

import edu.model.Jugador;
import edu.model.Validator;
import static spark.Spark.get;
import static spark.Spark.post;

public class Spark {
	
 	static Validator validador = new Validator();

    public static void main(String[] args) {

        get("/", (request, response) -> {
            Map<String, Object> model = new HashMap<>();
            model.put("welcome", "Bienvenido ");
            return new ModelAndView(model, "index.wm");
        }, new VelocityTemplateEngine());

 
        post("/partido", (request, response) -> {
       
          	String jugadorunonombre= request.queryParams("jugadoruno");
        	String jugadordosnombre= request.queryParams("jugadordos");
        	Jugador jugadoruno = new Jugador(jugadorunonombre);
        	Jugador jugadordos = new Jugador(jugadordosnombre);
       
            String marcador = validador.iniciarMarcador();
       
            Map<String, Object> model = respuesta(jugadoruno, jugadordos,
					marcador);
         
         
            
            return new ModelAndView(model, "partido.wm");
        }, new VelocityTemplateEngine());
        
        
        post("/puntojugadoruno", (request, response) -> {
         	String jugadorunonombre= request.queryParams("jugadoruno");
        	String jugadordosnombre= request.queryParams("jugadordos");
        	String jugadorunopos= request.queryParams("posjugadoruno");
        	String jugadordospos= request.queryParams("posjugadordos");
        	
        	Jugador jugadoruno = new Jugador(jugadorunonombre);
        	jugadoruno.setPosmarcador(Integer.parseInt(jugadorunopos));
        	
        	Jugador jugadordos = new Jugador(jugadordosnombre);
        	jugadordos.setPosmarcador(Integer.parseInt(jugadordospos));
       
            String marcador = validador.puntoParaJugadorUno(jugadoruno, jugadordos);
       
            Map<String, Object> model = respuesta(jugadoruno, jugadordos,
					marcador);
         
            return new ModelAndView(model, "partido.wm");
        }, new VelocityTemplateEngine());
        
        
        post("/puntojugadordos", (request, response) -> {
        	String jugadorunonombre= request.queryParams("jugadoruno");
        	String jugadordosnombre= request.queryParams("jugadordos");
        	String jugadorunopos= request.queryParams("posjugadoruno");
        	String jugadordospos= request.queryParams("posjugadordos");
        	
        	Jugador jugadoruno = new Jugador(jugadorunonombre);
        	jugadoruno.setPosmarcador(Integer.parseInt(jugadorunopos));
        	
        	Jugador jugadordos = new Jugador(jugadordosnombre);
        	jugadordos.setPosmarcador(Integer.parseInt(jugadordospos));
        	
       
            String marcador = validador.puntoParaJugadorDos(jugadoruno, jugadordos);
       
            
            Map<String, Object> model = respuesta(jugadoruno, jugadordos,
					marcador);
         
            return new ModelAndView(model, "partido.wm");
        }, new VelocityTemplateEngine());

    }

	private static Map<String, Object> respuesta(Jugador jugadoruno,
			Jugador jugadordos, String marcador) {
		Map<String, Object> model = new HashMap<>();       
		model.put("marcador",  marcador);
		model.put("versus", jugadoruno+" VS "+jugadordos);
		model.put("jugadoruno", jugadoruno);
		model.put("jugadordos", jugadordos);		
		model.put("posjugadoruno", jugadoruno.getPosmarcador());
		model.put("posjugadordos", jugadordos.getPosmarcador());
		return model;
	}

}
