package edu.model;

public class Jugador {
	
	private String nombre;
	
	private int posmarcador;
	
	private boolean gano;
	
	
	
	public Jugador(String nombre) {
		super();
		
		this.setPosmarcador(0);
		this.nombre=nombre;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getPosmarcador() {
		return posmarcador;
	}

	public void setPosmarcador(int posmarcador) {
		this.posmarcador = posmarcador;
	}

	@Override
	public String toString() {
		return  nombre ;
	}

	public boolean isGano() {
		return gano;
	}

	public void setGano(boolean gano) {
		this.gano = gano;
	}





	

}
