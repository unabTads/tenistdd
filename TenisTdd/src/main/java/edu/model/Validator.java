package edu.model;

public class Validator {

	public final static String[] marcadores = { "0", "15", "30", "40", "DEUCE",
			"GP", "-", "GANO" };

	public String puntoParaJugadorUno(Jugador jugadoruno, Jugador jugadordos) {


		if(validarSiYaGano(jugadoruno))
			return marcadores[7]+" "+jugadoruno;
		
		aumentarMarcador(jugadoruno);
		
		if(validarSiGano(jugadoruno))
			return marcadores[7]+" "+jugadoruno;

		return getMarcador(jugadoruno, jugadordos);

	}

	private boolean validarSiYaGano(Jugador jugador) {
		if(jugador.getPosmarcador() == 7){
			return true;
		}
		return false;
	}

	public String puntoParaJugadorDos(Jugador jugadoruno, Jugador jugadordos) {

		if(validarSiYaGano(jugadordos))
			return marcadores[7]+" "+jugadordos;
		aumentarMarcador(jugadordos);
		
		if (validarSiGano(jugadordos))
			return marcadores[7]+" "+jugadordos;
		

		return getMarcador(jugadoruno, jugadordos);

	}

	private String getMarcador(Jugador jugadoruno, Jugador jugadordos) {

		int posmarcadoruno = jugadoruno.getPosmarcador();
		int posmarcadordos = jugadordos.getPosmarcador();
		
		

		String marcadorjugadoruno = validadDeuce(marcadores[posmarcadoruno]);
		String marcadorjugadordos = validadDeuce(marcadores[posmarcadordos]);

		String marcador = marcadorjugadoruno + " - " + marcadorjugadordos;

		if (posmarcadoruno == posmarcadordos && posmarcadordos >= 3) {
			marcador = marcadores[4];
			jugadoruno.setPosmarcador(4);
			jugadordos.setPosmarcador(4);
		}
		
		
		
		
		return marcador;
	}

	private boolean validarSiGano(Jugador jugador) {
		if (jugador.getPosmarcador() == 6){
			jugador.setPosmarcador(7);
			return true;
		}
		if(jugador.getPosmarcador() == 4){
			jugador.setPosmarcador(7);
			return true;
		}
		
		if(jugador.getPosmarcador() == 7){
			return true;
		}
		
		return false;
	}

	private String validadDeuce(String marcador) {
		if (marcador.equals("DEUCE"))
			marcador = "-";
		return marcador;
	}

	private void aumentarMarcador(Jugador jugador) {
		int nuevoposmarcador = jugador.getPosmarcador() + 1;
		jugador.setPosmarcador(nuevoposmarcador);

	}

	public String iniciarMarcador() {

		return "0 - 0";

	}

}
