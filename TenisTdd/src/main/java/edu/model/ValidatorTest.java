package edu.model;

import org.junit.Assert;
import org.junit.Test;

public class ValidatorTest {

	Jugador jugadoruno = new Jugador("Nadal");
	Jugador jugadordos = new Jugador("Federer");
	Validator validator = new Validator();

	@Test
	public void deberiaIniciarEncuentro() {

		Validator validator = new Validator();

		// act
		String marcador = validator.iniciarMarcador();

		// assert
		Assert.assertEquals("0 - 0", marcador);
	}

	@Test
	public void deberiadar15puntosparajugador1() {

		jugadoruno.setPosmarcador(0);

		jugadordos.setPosmarcador(0);

	
		// act
		
		String marcador = validator.puntoParaJugadorUno(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("15 - 0", marcador);
	}
	

	@Test
	public void deberiadar15puntosparajugador2() {

		jugadoruno.setPosmarcador(0);

		jugadordos.setPosmarcador(0);
		
		
		// act
		
		String marcador = validator.puntoParaJugadorDos(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("0 - 15", marcador);
	}
	
	@Test
	public void siJugador1tiene30puntosdeberatener40() {

		jugadoruno.setPosmarcador(2);

		jugadordos.setPosmarcador(0);

		int comodin = 1;
		// act
	
		String marcador = validator.puntoParaJugadorUno(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("40 - 0", marcador);
	}
	
	
	@Test
	public void siJugador1tiene40puntosyJugador2tiene30Ypuntoparajugador2entoncesDeuce() {

		jugadoruno.setPosmarcador(3);

		jugadordos.setPosmarcador(2);

		int comodin = 2;
		// act
	
		String marcador = validator.puntoParaJugadorDos(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("DEUCE", marcador);
	}
	
	@Test
	public void siDeuceYpuntoparajugador1entoncesGP() {

		jugadoruno.setPosmarcador(4);

		jugadordos.setPosmarcador(4);

		int comodin = 1;
		// act
	
		String marcador = validator.puntoParaJugadorUno(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("GP - -", marcador);
	}
	
	
	@Test
	public void siJugadorUnotieneGPyPuntoparajugador2entoncesDeuce() {

		jugadoruno.setPosmarcador(5);

		jugadordos.setPosmarcador(4);

	
		// act
	
		String marcador = validator.puntoParaJugadorDos(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("DEUCE", marcador);
	}
	
	@Test
	public void siJugadorUnotieneGPyPuntoparajugador1entoncesWinjugador1() {

		jugadoruno.setPosmarcador(5);

		jugadordos.setPosmarcador(4);

		
		// act
	
		String marcador = validator.puntoParaJugadorUno(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("GANO "+ jugadoruno, marcador);
	}
	
	@Test
	public void siJugadorDostieneGPyPuntoparajugador1entoncesDeuce() {

		jugadoruno.setPosmarcador(4);

		jugadordos.setPosmarcador(5);

	
		// act
	
		String marcador = validator.puntoParaJugadorUno(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("DEUCE", marcador);
	}
	
	
	@Test
	public void siJugadorUnotiene40yPuntoparajugador1entoncesGano() {

		jugadoruno.setPosmarcador(3);

		jugadordos.setPosmarcador(2);

	
		// act
	
		String marcador = validator.puntoParaJugadorUno(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("GANO "+ jugadoruno, marcador);
	}
	
	@Test
	public void sivariosempatesEntoncesDeuce() {

		jugadoruno.setPosmarcador(4);

		jugadordos.setPosmarcador(4);

	
		// act
	
		validator.puntoParaJugadorUno(jugadoruno,
				jugadordos);
		

		validator.puntoParaJugadorDos(jugadoruno,
				jugadordos);
		

		validator.puntoParaJugadorDos(jugadoruno,
				jugadordos);
		

	String marcador=	validator.puntoParaJugadorUno(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("DEUCE", marcador);
	}

	
	@Test
	public void sialgunjugadoryaGanoNoHacerNada() {

		jugadoruno.setPosmarcador(0);

		jugadordos.setPosmarcador(7);

	
		// act

		

		

	String marcador=	validator.puntoParaJugadorDos(jugadoruno,
				jugadordos);

		// assert
		Assert.assertEquals("GANO "+ jugadordos, marcador);
	}

}
