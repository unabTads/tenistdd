package edu;

import com.codeborne.selenide.Selenide;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;


public class SmokeSteps {

	@Given("^ingreso al home$")
	public void ingreso_al_home() throws Throwable {
		  Selenide.open("http://localhost:4567/");
	}
	
	@When("^ingrese nombre jugadoruno  \"([^\"]*)\" y nombre jugadordos \"([^\"]*)\"$")
	public void ingrese_nombre_jugadoruno_y_nombre_jugadordos(String jugadoruno, String jugadordos) throws Throwable {
	    
		$(By.id("jugadoruno")).setValue(jugadoruno);
		$(By.id("jugadordos")).setValue(jugadordos);
	}

	@When("^presiono empezar$")
	public void presiono_empezar() throws Throwable {
		 $(By.id("empezar")).click();
	}
	@When("^presiono punto para jugadoruno$")
	public void presiono_punto_para_jugadoruno() throws Throwable {
	    // Express the Regexp above with the code you wish you had
		$(By.id("puntojugadoruno")).click();
	}

	@Then("^debo ver Vs \"([^\"]*)\"$")
	public void debo_ver_Vs(String message) throws Throwable {
	    // Express the Regexp above with the code you wish you had
		 $(By.id("mensaje")).shouldHave(text(message));
	}

	@Then("^ver marcador \"([^\"]*)\"$")
	public void ver_marcador(String message) throws Throwable {
	    // Express the Regexp above with the code you wish you had
		$(By.id("marcador")).shouldHave(text(message));
	}




}
